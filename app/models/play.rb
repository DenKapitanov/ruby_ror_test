class Play < ApplicationRecord
  belongs_to :game
  belongs_to :user

  has_many :tiles, autosave: true

  validates_associated :tiles
  validate :in_one_line
  validate :is_touches_tile_or_center

  def in_one_line
    if tiles.map(&:x).uniq.length != 1 && tiles.map(&:y).uniq.length != 1
      errors.add(:tiles, 'must be in one line')
    end
  end

  def is_touches_tile_or_center
    unless tiles.map(&:center?).any? || tiles.map(&:any_adjacent?).any?
      errors.add(:tiles, 'must touch existing tile or include the center square')
    end
  end

  def tile_at(x, y)
    self.tiles << Tile.new(x: x, y: y, play: self)
  end

  def tiles_at(coords)
    self.tiles = []
    coords.map { |a| self.tile_at(a[0], a[1]) }
  end
end
